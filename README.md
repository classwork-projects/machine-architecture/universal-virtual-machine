# Universal Virtual Machine

**Developed by Austin Agronick and Marcus Ferraro**  
Note: We did not receive any help outside of lecture, and did not collaborate with anyone else on this project.

This purpose of this project was to understand virtual-machine code (and by extension machine code) by writing a software implementation of a simple virtual machine.  
The optimized project can be found here: https://gitlab.com/classwork-projects/machine-architecture/universal-virtual-machine-optimized
<br />

## Architecture<br />

**Modules**

UM:

-   The UM is designed as a struct composed of an array of number registers, a counter, the available identifiers, and the available segments. This module contains the functions used to communicate instructions with the UM and the required memory segments. Being that memory segments have their own module (segments), it has secrets regarding memory segments. It also contains the functions for the 14 possible instructions that can be given.

Segments:

-   This module is responsible for creating and modifying segmented address space (storage). It is able to get all required attributes of a segment, as well as handle the memory for a given segment.
Being that memory segments are only shared with the UM module, its functions are considered private between the two modules.

Main:

-   The main is responsible only for reading a file and running the program accordingly. It does not have any secrets, as the required information is passed in as a file pointer, and the needed components for functionality are in their own modules.  

## Benchmark
The UM takes ~3 seconds to execute 50 million instructions. We tested using midmark.um (since it was the largest available file) which took an average of 4.871 seconds to complete, and had an average time of 16,423,732 instructions per second. At this rate, it would take 50,000,000/16423732 , which is approximately 3.044 seconds to execute 50 
million instructions. 

## Specification

### Machine State  
The UM has these components: 
 
-   Eight general-purpose registers holding one word each  
-   A very large address space that is divided into an ever-changing collection of *memory segments*. Each segment contains a sequence of words, and each is referred to by a distinct 32-bit identifier. The memory is *segmented* and *word-oriented*; you cannot load a byte
-   An I/O device capable of displaying ASCII characters and performing input and output of unsigned 8-bit characters  
-   A 32-bit program counter  

One distinguished segment is referred to by the 32-bit identifier 0 and stores the program. This segment is called the ‘0’ segment.   

### Notation
To describe the locations on the machine, we use the following notation:  
-   Registers are designated $r[0] through $r[7]  
-   The segment identified by the 32-bit number i is designated $m[i]. The ‘0’ segment is designated $m[0].  
-   A word at offset n within segment i is designated $m[i][n] You might refer to i as the segment number and n as the address within the segment.  

### Initial state 
The UM is initialized by providing it with a program, which is a sequence of 32-bit words. Initially:
-   The ‘0’ segment $m[0] contains the words of the program.  
-   A segment may be mapped or unmapped. Initially, $m[0] is mapped and all other segments are unmapped.  
-   All registers are zero.  
-   The program counter points to $m[0][0], i.e., the first word in the ‘0’ segment.  

### Execution cycle
At each time step, an instruction is retrieved from the word in the 0 segment whose address is the program counter. The program counter is advanced to the next word, if any, and the instruction is then executed.  

### Instructions’ coding and semantics 
The Universal Machine recognizes 14 instructions. The instruction is coded by the four most significant bits of the instruction word. These bits are called the opcode.  

| Opcode 	| Operator         	| Action 	                                    |
|--------	|------------------	|--------	                                    |
| 0      	| Conditional Move 	| if $r[C]6= 0 then $r[A] := $r[B]              |
| 1      	| Segmented Load    | $m[$r[A]][$r[B]] := $r[C]      	            |
| 2      	| Segmented Store   | $m[$r[A]][$r[B]] := $r[C]      	            |
| 3      	| Addition          | $r[A] := ($r[B] + $r[C]) mod 232       	    |
| 4      	| Multiplication    | $r[A] := ($r[B]×$r[C]) mod 232            	|
| 5      	| Division          | $r[A] := ($r[B]÷$r[C]) (integer division)     |
| 6      	| Bitwise NAND      | r[A] :=¬($r[B]∧$r[C])       	                |
| 7      	| Halt              | Computation stops       	                    |
| 8      	| Map segment       | A new segment is created with a number of words equal to the value in $r[C]. Each word in the new segment is initialized to zero. A bit pattern that is not all zeroes and does not identify any currently mapped segment is placed in $r[B]. The new segment is mapped as $m[$r[B]].       	                                                             |
| 9      	| Unmap segment     | The segment $m[$r[C]] is unmapped.       	    |
|        	|                  	| Future Map Segment instructions may reuse the identifier $r[C]. |
| 10       	| Output            | The value in $r[C] is displayed on the I/O device immediately. Only values from 0 to 255 are allowed.                                          |
| 11       	| Input             | The UM waits for input on the I/O device. When input arrives, $r[c] is loaded with the input, which must be a value from 0 to 255. If the end of input has been signaled, then $r[C] is loaded with a full 32-bit word in which every bit is 1  |
| 12      	| Load Program      | Segment $m[$r[B]] is duplicated, and the duplicate replaces $m[0], which is abandoned. The program counter is set to point to $m[0][$r[C]]. If $r[B]=0, the load program operation should be extremely quick, as this is effectively a jump      |
| 13       	| Load Value        | See semantics for “other instruction”.       	|


**Three-register instructions**    
Most instructions operate on three registers. The registers are identified by number; we’ll call the numbers A, B, and C. Each number coded as a three-bit unsigned integer embedded in the instruction word. The register C is coded by the three least significant bits, the register B by the three next more significant than those, and the register A by the three next more significant than those:  

**One other instruction**  
One special intruction, with opcode 13, does not describe registers in the same way as the others. Instead, the three bits immediately less significant than opcode describe a single register A. The remaining 25 bits indicate a value, which is loaded into $r[A].    

### Failure modes  
The behavior of the Universal Machine is not fully defined; under circumstances detailed below (and only these circumstances), the machine may fail.  
-   If at the beginning of a machine cycle the program counter points outside the bounds of $m[0], the machine may fail.  
-   If at the beginning of a cycle, the word pointed to by the program counter does not code for a valid instruction, the machine may fail.  
-   If a segmented load or segmented store refers to an unmapped segment, the machine may fail.  
-   If a segmented load or segmented store refers to a location outside the bounds of a mapped segment, the machine may fail.  
-   If an instruction unmaps $m[0], or if it unmaps a segment that is not mapped, the machine may fail.  
-   If an instruction divides by zero, the machine may fail.  
-   If an instruction loads a program from a segment that is not mapped, then the machine may fail.  
-   If an instruction outputs a value larger than 255, the machine may fail.  
In the interests of performance, failure may be treated as an unchecked run-time error. Even a core dump is OK, though a segfault is not.  

**Resource exhaustion**  
If a UM program demands resources that your implementation is not able to provide, and if the demand does not constitute failure as defined above, your only recourse is to halt execution with a checked run-time error.
