#ifndef SEGMENTS_INCLUDED
#define SEGMENTS_INCLUDED

#include <stdint.h>

#define T Seg_T
typedef struct Seg_T *Seg_T;

Seg_T            new_Seg   (int size, uint32_t identifier); //Create a new seg
extern void      free_Seg  (Seg_T *seg);                // deallocate seg memory
extern int       get_Size(Seg_T seg);                   //returns the size of the provided seg

uint32_t  Seg_map   (Seg_T seg, int size);      //maps seg if it is unmapped
extern void      Seg_unmap (Seg_T seg);                //unmaps a mapped seg
extern bool      is_Mapped(Seg_T seg);

uint32_t  get_word(Seg_T seg, unsigned index);         //Get the word at a specified index of a seg
void insert_word(Seg_T seg, unsigned index, uint32_t word); //insert a given word into the seg at the given index

uint32_t get_identifier(Seg_T seg); 	               // returns identifier of the given seg




#undef T
#endif
