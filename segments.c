#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <inttypes.h>
#include "assert.h"
#include "segments.h"


struct Seg_T {
        bool            is_Mapped;
        unsigned        size;
        uint32_t        identifier;
        uint32_t        *words; 
};


Seg_T new_Seg(int size, uint32_t identifier)
{
        Seg_T seg = malloc(sizeof(*seg));       //allocate memory for seg

        assert(seg != NULL);

        // initial properties
        seg->is_Mapped = true; 
        seg->size = size;
        seg->identifier = identifier;
        seg->words = (uint32_t *)malloc(sizeof(uint32_t) * size);   //allocate space for words
        

        // Each word in the new seg is initialized to zero
        for (int i = 0; i < size; i++) 
        {
                (seg->words)[i] = 0; 
        }

        //everything initialized, return new seg
        return seg; 
}


void free_Seg(Seg_T *seg)
{
        assert(seg && *seg);    //assert that a valid seg has been passed
        free((*seg)->words);    //clear words first
        free(*seg);
}


uint32_t get_word(Seg_T seg, unsigned index)
{
        assert(seg && seg->is_Mapped && seg->size > index);  //make sure seg is valid, and is_Mapped is true, and bounds valid
        return  (seg->words)[index];

}


void insert_word(Seg_T seg, unsigned index, uint32_t word)
{
        assert(seg && seg->is_Mapped && seg->size > index);  //make sure seg is valid, and is_Mapped is true, and bounds valid
        (seg->words)[index] = word;
}


int get_Size(Seg_T seg)
{
        assert(seg && seg->is_Mapped);  //make sure seg is valid, and is_Mapped is true
        return seg->size;
}


uint32_t get_identifier(Seg_T seg)
{
        assert(seg && seg->is_Mapped);  //make sure seg is valid, and is_Mapped is true
        return seg->identifier;
}



uint32_t Seg_map(Seg_T seg, int size)
{
        assert(seg);    //make sure user passed valid seg
        if (seg->is_Mapped) return false;   // can't map an already mapped seg

        free(seg->words);
      
      
        // initial properties
        seg->is_Mapped = true; 
        seg->size = size;
        seg->words = (uint32_t *)malloc(sizeof(uint32_t) * size);


        // Each word in the new seg is initialized to zero
        for (int i = 0; i < size; i++)
        {
                (seg->words)[i] = 0;
        }

        return true;
}

// unmap given seg
void Seg_unmap(Seg_T seg)
{
        assert(seg);
        seg->is_Mapped = false;
}

// return true if seg is already mapped
bool is_Mapped(Seg_T seg)
{
        assert(seg);
        return seg->is_Mapped;
}
