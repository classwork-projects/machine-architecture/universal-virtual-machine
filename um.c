#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include "bitpack.h"
#include "assert.h"
#include "um.h"
#include "segments.h"
#include "stack.h"
#include "seq.h"

// globals
#define NUM_REGISTERS 8

static uint32_t zero = 0;
static uint64_t one = 1;


// universal machine struct
struct UM_T {
    uint32_t registers[NUM_REGISTERS];
    uint32_t counter;
    Stack_T  available_IDs;
    Seq_T    segments;
};

// instructions indexable with the opcode
static void (*instructions[13]) (UM_T UM, int rA, int rB, int rC) = {
        conditional_move,
        segmented_load,
        segmented_store,
        addition,
        multiplication,
        division,
        bitwise_NAND,
        halt,
        map_segment,
        unmap_segment,
        output,
        input,
        load_program,
};


/* UNIVERSAL MACHINE */

// given a UM struct and segment ID, return a particular segment from the sequence
static inline Seg_T get_seg(UM_T UM, unsigned segmentID)
{
    return Seq_get(UM->segments, segmentID);
}

// initialize a new UM with no program instructions
UM_T new_UM()
{
    UM_T um = malloc(sizeof(*um));
    assert(um != NULL);

    um->counter = 0;
    um->available_IDs = Stack_new();

    for (int i = 0; i < NUM_REGISTERS; i++)
    {
        (um->registers)[i] = 0;
    }

    // initialize 0 segment m[0]
    um->segments = Seq_new(1000);
    Seq_addhi(um->segments, new_Seg(5, 0));

    return um;
}

// gets a word from a given segment
uint32_t get_seg_word(UM_T UM, uint32_t segmentID, unsigned offset)
{
    // verify UM exists, the segmentID is valid, and indexes a mapped segment
    assert(UM && (uint32_t)Seq_length(UM->segments) > segmentID && is_Mapped(get_seg(UM, segmentID)));
    return get_word(get_seg(UM, segmentID), offset);
}

// sets a word value for the given segment
void edit_seg_word(UM_T UM, uint32_t segmentID, unsigned offset, uint32_t word)
{
    // verify UM exists, the segmentID is valid, and indexes a mapped segment
    assert(UM && (uint32_t)Seq_length(UM->segments) > segmentID && is_Mapped(get_seg(UM, segmentID)));
    insert_word(get_seg(UM, segmentID), offset, word);
}

// map a segment of given size - number of words
uint32_t map_UM_seg(UM_T UM, unsigned num_words)
{
    assert(UM);
    // get ID from stack and map segment
    if (Stack_empty(UM->available_IDs) == 0)
    {
        uint32_t segmentID = (uint32_t)(uintptr_t)Stack_pop(UM->available_IDs);
        Seg_T seg = get_seg(UM, segmentID);
        Seg_map(seg, num_words);
        return segmentID;
    }
    // add new segment
    Seg_T new_seg = new_Seg(num_words, Seq_length(UM->segments));
    Seq_addhi(UM->segments, new_seg);

    return get_identifier(new_seg);
}

// unmaps given segment, rendering it unusable to the UM
void unmap_UM_seg(UM_T UM, uint32_t segmentID)
{
    // verify UM exists and the segmentID is valid
    assert(UM && (uint32_t)Seq_length(UM->segments) > segmentID);
    // unmap
    Seg_unmap(get_seg(UM, segmentID));
    // make segmentID available
    Stack_push(UM->available_IDs, (void *)(uintptr_t)segmentID);
}

/* gets the value of a specified register */
uint32_t get_register_val(UM_T UM, unsigned reg)
{
    // verify UM exists and the register number is valid
    assert(UM && reg < 8);
    return (UM->registers)[reg];
}

// change the given registers value
void change_register_word(UM_T UM, unsigned reg, uint32_t word)
{
    // verify UM exists and the register number is valid
    assert(UM && reg < 8);
    (UM->registers)[reg] = word;
}

// read in the file containing a UM program to run and store these instructions in m[0]
void read_UM(UM_T UM, FILE *fp)
{
    // read each instruction to new sequence
    Seq_T instructions = Seq_new(5);
    while (true)
    {
        uint32_t instruction = 0;
        // read and bitpack 4 characters from input line
        for (int i = 3; i >= 0; i--)
        {
            int c = fgetc(fp);
            if (c == EOF)
            {
                break;
            }
            instruction = Bitpack_newu(instruction, 8, i*8, c);
        }

        // stop reading if end of file
        if (feof(fp) != 0)
        {
            break;
        }
        // else add instruction to program instructions sequence
        Seq_addhi(instructions, (void *)(uintptr_t)instruction);
    }

    // remap segment 0 for instructions
    unmap_UM_seg(UM, 0);
    map_UM_seg(UM, Seq_length(instructions));

    // put each instruction from sequence in segment 0
    for (int i = 0; i < Seq_length(instructions); i++)
    {
        uint32_t instruction = (uint32_t)(uintptr_t)Seq_get(instructions, i);
        edit_seg_word(UM, 0, i, instruction);
    }

    // free instructions sequence
    Seq_free(&instructions);
}

// run the supplied program to completion
void run_UM(UM_T UM)
{
    while (true) {
        // verify program counter is within m[0]
        assert(UM->counter <= (uint32_t)get_Size(get_seg(UM, 0)));

        // get instruction and it's opcode
        uint32_t instruction = get_seg_word(UM, 0, UM->counter);
        int opcode = Bitpack_getu(instruction, 4, 28);

        // verify opcode
        assert(opcode >= 0 && opcode <= 13);

        if (opcode == 13)   // instruction: load_value
        {
            int regA  = Bitpack_getu(instruction, 3, 25);
            int value = Bitpack_getu(instruction, 25, 0);
            load_value(UM, regA, value);
        }
        else if (opcode == 7)   // instruction: halt
        {
            break;

        }
        else {  // other instructions
            int regA = Bitpack_getu(instruction, 3, 6);
            int regB = Bitpack_getu(instruction, 3, 3);
            int regC = Bitpack_getu(instruction, 3, 0);
            instructions[opcode](UM, regA, regB, regC);
        }

        // instruction: load_program (adjust counter)
        if (opcode == 12)
        {
            int regC = Bitpack_getu(instruction, 3, 0);
            UM->counter = (UM->registers)[regC];
        }
        else { // other instructions
            (UM->counter)++;
        }
    }
}


// free all UM memory
void free_UM(UM_T *UM_p)
{
    assert(UM_p && *UM_p);
    UM_T um = *UM_p;

    // free all segments
    for (int i = 0; i < Seq_length(um->segments); i++)
    {
        Seg_T seg = get_seg(um, i);
        free_Seg(&seg);
    }
    // free sequence and UM struct
    Seq_free(&(um->segments));
    free(um);
}


/* INSTRUCTIONS */

// moves value between two registers
void conditional_move(UM_T UM, int rA, int rB, int rC)
{
    if (get_register_val(UM, rC) != 0)
    {
        change_register_word(UM, rA, get_register_val(UM, rB));
    }
}

// loads a value from given segment, stores into the given register
void segmented_load(UM_T UM, int rA, int rB, int rC)
{
    uint32_t rB_val = get_register_val(UM, rB);
    uint32_t rC_val = get_register_val(UM, rC);
    change_register_word(UM, rA, get_seg_word(UM, rB_val, rC_val));
}

// loads a value from given register, stores into the given segment
void segmented_store(UM_T UM, int rA, int rB, int rC)
{
    uint32_t rA_val = get_register_val(UM, rA);
    uint32_t rB_val = get_register_val(UM, rB);
    uint32_t rC_val = get_register_val(UM, rC);
    edit_seg_word(UM, rA_val, rB_val, rC_val);
}

// addition of values stored in two registers
void addition(UM_T UM, int rA, int rB, int rC)
{
    uint32_t rB_val = get_register_val(UM, rB);
    uint32_t rC_val = get_register_val(UM, rC);
    uint32_t sum = (rB_val + rC_val) % (one << 32);
    change_register_word(UM, rA, sum);
}

// multiplication of values stored in two register
void multiplication(UM_T UM, int rA, int rB, int rC)
{
    uint32_t rB_val = get_register_val(UM, rB);
    uint32_t rC_val = get_register_val(UM, rC);
    uint32_t product = (rB_val * rC_val) % (one << 32);
    change_register_word(UM, rA, product);
}

// division of values stored in two registers
void division(UM_T UM, int rA, int rB, int rC)
{
    uint32_t rB_val = get_register_val(UM, rB);
    uint32_t rC_val = get_register_val(UM, rC);
    assert(rC_val != 0);    // verify divisor is not 0
    uint32_t quotient = (rB_val / rC_val);
    change_register_word(UM, rA, quotient);
}

// returns a NAND on two values in two registers
void bitwise_NAND(UM_T UM, int rA, int rB, int rC)
{
    uint32_t rB_val = get_register_val(UM, rB);
    uint32_t rC_val = get_register_val(UM, rC);
    uint32_t NAND = ~(rB_val & rC_val);
    change_register_word(UM, rA, NAND);
}

// signal end of program
void halt(UM_T UM, int rA, int rB, int rC)
{
    // do nothing
    (void) UM;
    (void) rA;
    (void) rB;
    (void) rC;
}

// maps a new segment with given length and stores segmentID into a register
void map_segment(UM_T UM, int rA, int rB, int rC)
{
    (void) rA;
    change_register_word(UM, rB, map_UM_seg(UM, get_register_val(UM, rC)));
}

// unmaps a specified segment
void unmap_segment(UM_T UM, int rA, int rB, int rC)
{
    (void) rA;
    (void) rB;
    int segmentID = get_register_val(UM, rC);
    assert(segmentID != 0); // verify not unmapping m[0]
    unmap_UM_seg(UM, segmentID);
}

// outputs a ASCII value from given register
void output(UM_T UM, int rA, int rB, int rC)
{
    (void) rA;
    (void) rB;
    uint32_t c = get_register_val(UM, rC);
    // unsigned c is always >= 0
    if (c <= 255)
    {
        putchar(c);
    }
}

// read in a character from input and store in a register
void input(UM_T UM, int rA, int rB, int rC)
{
    (void) rA;
    (void) rB;
    char c = getchar();
    if (c == EOF)
    {
        change_register_word(UM, rC, (~zero));
    }
    else {
        change_register_word(UM, rC, c);
    }
}

// load given memory segment into m[0]
void load_program(UM_T UM, int rA, int rB, int rC)
{
    (void) rA;
    (void) rC;

    // if loading m[0], skip
    if (get_register_val(UM, rB) == 0)
    {
        return;
    }

    // get segment m[rB_val] length
    uint32_t segmentID = get_register_val(UM, rB);
    // verify UM exists and the segmentID is valid
    assert(UM && (uint32_t)Seq_length(UM->segments) > segmentID);
    int seg_length = get_Size(get_seg(UM, segmentID));

    // unmap m[0] and remap with new length from m[rB_val]
    unmap_UM_seg(UM, 0);
    map_UM_seg(UM, seg_length);

    // copy contents of m[rB_val] into m[0]
    for (int i = 0; i < seg_length; i++)
    {
        uint32_t word = get_seg_word(UM, segmentID, i);
        edit_seg_word(UM, 0, i, word);
    }
}

// load a value and stores in given register
void load_value(UM_T UM, int rA, int value)
{
    change_register_word(UM, rA, value);
}