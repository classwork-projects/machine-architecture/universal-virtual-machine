#include <stdio.h>
#include <stdlib.h>
#include "um.h"

// reads file given on the command line
FILE *read_file(int argc, char *argv[])
{
    if (argc != 2)
    {
        fprintf(stderr, "ERROR: Incorrect number of arguments\n");
        return NULL;
    }

    FILE *fp = fopen(argv[1], "rb");
    if (fp == NULL)
    {
        fprintf(stderr, "ERROR: File does not exist\n");
        return NULL;
    }

    return fp;
}


int main(int argc, char *argv[])
{
    FILE *fp = read_file(argc, argv);
    if (fp == NULL)
    {
        return EXIT_FAILURE;
    }


    UM_T um = new_UM();
    read_UM(um, fp);
    run_UM(um);
    free_UM(&um);

    fclose(fp);
}

