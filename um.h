#ifndef UM_INCLUDED
#define UM_INCLUDED

#include <inttypes.h>

typedef struct UM_T *UM_T;

UM_T     new_UM();
uint32_t get_seg_word(UM_T UM, uint32_t segID, unsigned offset);
void     edit_seg_word(UM_T UM, uint32_t segID, unsigned offset, uint32_t word);
uint32_t map_UM_seg(UM_T UM, unsigned numWords);
void     unmap_UM_seg(UM_T UM, uint32_t segID);
uint32_t get_register_val(UM_T UM, unsigned reg);
void     change_register_word(UM_T UM, unsigned reg, uint32_t word);
void     read_UM(UM_T UM, FILE *fp);
void     run_UM(UM_T UM);
void     free_UM(UM_T *UM);

void conditional_move(UM_T UM, int rA, int rB, int rC);
void segmented_load(UM_T UM, int rA, int rB, int rC);
void segmented_store(UM_T UM, int rA, int rB, int rC);
void addition(UM_T UM, int rA, int rB, int rC);
void multiplication(UM_T UM, int rA, int rB, int rC);
void division(UM_T UM, int rA, int rB, int rC);
void bitwise_NAND(UM_T UM, int rA, int rB, int rC);
void halt(UM_T UM, int rA, int rB, int rC);
void map_segment(UM_T UM, int rA, int rB, int rC);
void unmap_segment(UM_T UM, int rA, int rB, int rC);
void output(UM_T UM, int rA, int rB, int rC);
void input(UM_T UM, int rA, int rB, int rC);
void load_program(UM_T UM, int rA, int rB, int rC);
void load_value(UM_T UM, int rA, int value);

#endif
